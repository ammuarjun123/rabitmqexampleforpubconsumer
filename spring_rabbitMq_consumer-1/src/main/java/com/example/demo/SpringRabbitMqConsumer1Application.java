package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRabbitMqConsumer1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitMqConsumer1Application.class, args);
	}

}
